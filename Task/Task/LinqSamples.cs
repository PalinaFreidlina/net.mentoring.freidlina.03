﻿// Copyright © Microsoft Corporation.  All Rights Reserved.
// This code released under the terms of the 
// Microsoft Public License (MS-PL, http://opensource.org/licenses/ms-pl.html.)
//
//Copyright (C) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using SampleSupport;
using Task.Data;

// Version Mad01

namespace SampleQueries
{
	[Title("LINQ Module")]
	[Prefix("Linq")]
	public class LinqSamples : SampleHarness
	{

		private DataSource dataSource = new DataSource();

		[Category("Restriction Operators")]
		[Title("Where - Task 1")]
		[Description("This sample uses the where clause to find all elements of an array with a value less than 5.")]
		public void Linq1()
		{
			int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

			var lowNums =
				from num in numbers
				where num < 5
				select num;

			Console.WriteLine("Numbers < 5:");
			foreach (var x in lowNums)
			{
				Console.WriteLine(x);
			}
		}

		[Category("Restriction Operators")]
		[Title("Where - Task 2")]
		[Description("This sample return return all presented in market products")]

		public void Linq2()
		{
			var products =
				from p in dataSource.Products
				where p.UnitsInStock > 0
				select p;

			foreach (var p in products)
			{
				ObjectDumper.Write(p);
			}
		}

	    [Category("HomeWork")]
	    [Title("Task 01")]
	    [Description("Выдайте список всех клиентов, чей суммарный оборот (сумма всех заказов) превосходит некоторую величину X. " + 
            "Продемонстрируйте выполнение запроса с различными X (подумайте, можно ли обойтись без копирования запроса несколько раз)")]
	    public void Linq01()
	    {
	        for (int i = 1000; i <= 1000000; i *= 10)
	        {
                Console.WriteLine($@"minTotal = {i}");
	            decimal total = 0;
	            var customerOrdersTotal = from p in dataSource.Customers
	                where (total = p.Orders.Sum(x => x.Total)) > i
	                select new { CustomerId = p.CustomerID, TotalOrdersCost = total };
                ObjectDumper.Write(customerOrdersTotal);
	            Console.WriteLine($"Total: {customerOrdersTotal.Count()}");
            }
        }
        
        [Category("HomeWork")]
	    [Title("Task 02")]
	    [Description("Для каждого клиента составьте список поставщиков, находящихся в той же стране и том же городе. " +
	                 "Сделайте задания с использованием группировки и без.")]
	    public void Linq02()
        {
            var clients = from c in dataSource.Customers
                from s in dataSource.Suppliers
                where c.Country == s.Country && c.City == s.City
                select new {c.CustomerID, s.SupplierName};
            Console.WriteLine("Without groupping");
            ObjectDumper.Write(clients);
	        Console.WriteLine($"Total: {clients.Count()}");

            var clients1 = from c in dataSource.Customers
                from s in dataSource.Suppliers
                where c.Country == s.Country && c.City == s.City
                group s by c.CustomerID
                into sid
                select new {Customer = sid.Key, Suppliers = string.Join(", ", sid.Select(s => s.SupplierName))};

            Console.WriteLine("\nWith groupping");
            ObjectDumper.Write(clients1);
            Console.WriteLine($"Total: {clients1.Count()}");
        }

	    [Category("HomeWork")]
	    [Title("Task 03")]
	    [Description("Найдите всех клиентов, у которых были заказы, превосходящие по сумме величину X")]
	    public void Linq03()
	    {
	        int minTotal = 10000;
	        var orderCost = 0m;
	        var clients = from p in dataSource.Customers
	            where p.Orders.Any(x => (orderCost = x.Total) > minTotal)
	            select new {CustomerId = p.CustomerID, OrderCost = orderCost};

            ObjectDumper.Write(clients);
	        Console.WriteLine($"Total: {clients.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 04")]
	    [Description("Выдайте список клиентов с указанием, начиная с какого месяца какого года они стали клиентами (принять за таковые месяц и год самого первого заказа)")]
	    public void Linq04()
	    {
	        var clients = dataSource.Customers.Where(c=>c.Orders.Any()).Select(c =>
	            new {Customer = c.CustomerID, Firstorder = c.Orders.Min(o => o.OrderDate).Year});

	        ObjectDumper.Write(clients);
	        Console.WriteLine($"Total: {clients.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 05")]
	    [Description("Сделайте предыдущее задание, но выдайте список отсортированным по году, месяцу, оборотам клиента (от максимального к минимальному) и имени клиента")]
	    public void Linq05()
	    {
	        var clients = dataSource.Customers.Where(c => c.Orders.Any()).Select(c =>
	                new
	                {
	                    c.CustomerID,
	                    CustomerSumTotal = c.Orders.Sum(o => o.Total),
	                    Firstorder = c.Orders.Min(o => o.OrderDate)
	                }).OrderBy(c => c.Firstorder.Year).ThenBy(c => c.Firstorder.Month)
	            .ThenByDescending(c => c.CustomerSumTotal).ThenBy(c => c.CustomerID);

	        ObjectDumper.Write(clients);
	        Console.WriteLine($"Total: {clients.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 06")]
	    [Description("Укажите всех клиентов, у которых указан нецифровой почтовый код или не заполнен регион или в телефоне не указан код оператора " + 
            "(для простоты считаем, что это равнозначно «нет круглых скобочек в начале»).")]
	    public void Linq06()
	    {
	        var clients = dataSource.Customers
	            .Where(c => (c.PostalCode != null && c.PostalCode.Any(char.IsLetter)) || string.IsNullOrEmpty(c.Region) ||
	                        !c.Phone.StartsWith("(")).Select(c => new {c.CustomerID, c.PostalCode, c.Region, c.Phone});

	        ObjectDumper.Write(clients);
	        Console.WriteLine($"Total: {clients.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 07")]
	    [Description("Сгруппируйте все продукты по категориям, внутри – по наличию на складе, внутри последней группы отсортируйте по стоимости")]
	    public void Linq07()
	    {
	        var products = from p in dataSource.Products
	            group p by p.Category
	            into pr
	            select new
	            {
	                Category = pr.Key,
	                Products = string.Join(", ",
	                    from p1 in pr
	                    group p1 by p1.UnitsInStock
	                    into pr1
	                    select new
	                    {
	                        UnitsInStock = pr1.Key,
	                        Prices = string.Join(", ",
	                            from pr2 in pr1
	                            orderby pr2.UnitPrice
	                            select new {Price = pr2.UnitPrice})
	                    })
	            };

            ObjectDumper.Write(products);
	        Console.WriteLine($"Total: {products.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 08")]
	    [Description("Сгруппируйте товары по группам «дешевые», «средняя цена», «дорогие». Границы каждой группы задайте сами")]
	    public void Linq08()
	    {
	        decimal maxCheep = 20;
	        decimal minExpencive = 100;
	        var products = from p1 in (from p in dataSource.Products
	                select new
	                {
	                    Category = p.UnitPrice < minExpencive
	                        ? p.UnitPrice > maxCheep
	                            ? "Middle price"
	                            : "Cheep"
	                        : "Expensive",
	                    Product = p.ProductName
	                })
	            group p1 by p1.Category
	            into pr
	            select new {Category = pr.Key, Products = string.Join(", ", pr.Select(p => p.Product))};

            ObjectDumper.Write(products);
	        Console.WriteLine($"Total: {products.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 09")]
	    [Description("Рассчитайте среднюю прибыльность каждого города (среднюю сумму заказа по всем клиентам из данного города) " + 
            "и среднюю интенсивность (среднее количество заказов, приходящееся на клиента из каждого города)")]
	    public void Linq09()
	    {
	        var orders = dataSource.Customers.GroupBy(c => c.City)
	            .Select(cc => new {City = cc.Key, Orders = cc.Select(c => new {c.Orders})}).Select(co =>
	                new
	                {
	                    co.City,
	                    Average = decimal.Round(
	                        co.Orders.Select(o => o.Orders.Any() ? o.Orders.Average(x => x.Total) : 0).Average(), 2),
	                    Intencivity = Math.Round(co.Orders.Average(o => o.Orders.Length),2)
	                });

	        ObjectDumper.Write(orders);
	        Console.WriteLine($"Total: {orders.Count()}");
	    }

	    [Category("HomeWork")]
	    [Title("Task 10")]
	    [Description("Сделайте среднегодовую статистику активности клиентов по месяцам (без учета года), " + 
            "статистику по годам, по годам и месяцам (т.е. когда один месяц в разные годы имеет своё значение).")]
	    public void Linq10()
	    {
	        var staticticsYear = dataSource.Customers
	            .SelectMany(c => c.Orders, (name, order) => new {Customer = name, Order = order})
	            .GroupBy(c => c.Order.OrderDate.Year)
                .Select(x => new {Year = x.Key, Orders = x.Count()})
	            .OrderBy(x => x.Year);

            Console.WriteLine("Year statistics");
	        ObjectDumper.Write(staticticsYear);
	        Console.WriteLine($"Total: {staticticsYear.Count()}");

	        var staticticsMonth = dataSource.Customers
	            .SelectMany(c => c.Orders, (name, order) => new {Customer = name, Order = order})
	            .GroupBy(c => c.Order.OrderDate.Month)
                .Select(x => new {Month = x.Key, Orders = x.Count()})
	            .OrderBy(x => x.Month);

	        Console.WriteLine("Month statistics");
	        ObjectDumper.Write(staticticsMonth);
	        Console.WriteLine($"Total: {staticticsMonth.Count()}");

	        var staticticsMonthYear = dataSource.Customers
	            .SelectMany(c => c.Orders, (name, order) => order.OrderDate)
	            .GroupBy(c => c.Month)
	            .Select(x => new {Month = x.Key, OrdersDates = x})
	            .SelectMany(c => c.OrdersDates, (month, order) => new {month.Month, order})
	            .GroupBy(o => o.order.Year)
	            .Select(x => new {Year = x.Key, Orders = x})
	            .SelectMany(o => o.Orders,
	                (month, order) => new
	                {
	                    month.Year,
	                    order.Month,
	                    Orders = month.Orders.Count(x => x.Month == order.Month)
	                })
	            .Distinct()
	            .OrderBy(o => o.Year)
	            .ThenBy(x => x.Month);

	        Console.WriteLine("Month and Year statistics");
	        ObjectDumper.Write(staticticsMonthYear);
	        Console.WriteLine($"Total: {staticticsMonthYear.Count()}");
        }
    }
}
